﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentClass
{
    class Person
    {
        public string firstName { get; set; }
        public string lastName { get; set; }

        private int _age;
        public int age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value<=0)
                {
                    value = 1;
                }
                _age = value;
            }
        }

        public string cf
        {
            get
            {
                return (firstName + lastName + age.ToString()); 
            }
        }

        public Person(string firstName, string lastName, int age)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
        }

        public string describe()
        {
            string data = "Fist name: " + firstName + "\r\n " +
                            "Last name: " + lastName + "\r\n " +
                            "Age: " + age.ToString() + "\r\n " +
                             "CF :" + cf;
            return data;
        }
       
    }
}
