﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentClass
{
    class Student:Person
    {
        public string section
        {
            get; set;
        }

        public Student(string firstName, string lastName, int age, string section): base(firstName, lastName, age)
        {
            this.section = section;
        }
    }
}
